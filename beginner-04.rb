class Player
  def play_turn(warrior)
    if @health == nil
      @health = warrior.health
    end
    if warrior.feel.enemy?
      warrior.attack!
    elsif @health > warrior.health
      warrior.walk!
    elsif warrior.feel.empty?
      if warrior.health < 20
        warrior.rest!
      else
        warrior.walk!
      end
    end
    @health = warrior.health
  end
end