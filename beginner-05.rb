class Player
  def play_turn(warrior)
    if @health == nil
      @health = warrior.health
    end
    if warrior.feel.enemy?
      warrior.attack!
    elsif warrior.feel.captive?
      warrior.rescue!
    elsif @health > warrior.health
      warrior.walk!
    elsif warrior.feel.empty?
      if warrior.health < 20
        warrior.rest!
      else
        warrior.walk!
      end
    end
    @health = warrior.health
  end
end